class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.migrate :db

      t.timestamps null: false
    end
  end
end
